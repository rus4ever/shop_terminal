import React, {useEffect, useState, useContext} from 'react';
import {StyleSheet, View, Alert, Switch} from 'react-native';
import {BasketData, BasketMethods} from './data/data';
import {useHistory, useRouteMatch} from 'react-router-native';

import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Body,
  Icon,
  Text,
  Card,
} from 'native-base';

import {AppContext} from '../Context/AppContext';

const Checkout: React.FC<{data: BasketData & BasketMethods}> = (props) => {
  const [sum, setSum] = useState<null | number>(null);
  const needPassport = props.data.products.reduce(
    (acc, curr) => acc || curr.passport,
    false,
  );
  const {path} = useRouteMatch();
  const payOffline = () => {
    history.push(`${path}/pay/offline`);
  };
  const payOnline = () => {
    history.push(`${path}/pay/online`);
  };

  const checkout = async () => {
    try {
      const res = await props.data.checkout();
      setSum(res);
    } catch (e) {
      Alert.alert('Произошла ошибка, повторите попытку позже');
    }
  };
  useEffect(() => {
    checkout();
  }, []);
  const appContext = useContext(AppContext);
  const history = useHistory();

  const [useBonuses, setUseBonuses] = useState(false);
  const toggleBonuses = () => {
    setUseBonuses(!useBonuses);
  };
  return (
    <Container>
      <Header>
        <Left>
          <Button onPress={() => history.goBack()} transparent>
            <Icon name="arrow-back" />
            <Text>Назад</Text>
          </Button>
        </Left>
        <Body>
          <Title>Оплата</Title>
        </Body>
        <Left />
      </Header>
      <Content padder>
        <View
          style={{
            backgroundColor: '#FFF1FF',
            margin: 10,
            paddingVertical: 25,
            paddingHorizontal: 17,
          }}>
          <Text style={{marginLeft: 'auto', fontSize: 24}}>
            У вас {appContext.bonusCount} баллов
          </Text>
        </View>
        {sum === null ? (
          <Text> Обработка финальной суммы покупок</Text>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              margin: 10,
            }}>
            <Text style={{fontSize: 24}}>Сумма к оплате</Text>
            <Text style={{fontSize: 24}}>
              {Math.max(
                sum / 100 - (useBonuses ? appContext.bonusCount : 0),
                0,
              ).toFixed(2)}{' '}
              руб.
            </Text>
          </View>
        )}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: 10,
            marginVertical: 16,
            borderBottomColor: 'rgba(0, 0, 0, 0.12);',
            paddingBottom: 10,
            borderBottomWidth: 1,
          }}>
          <View style={styles.iconWrapper}>
            <Switch
              trackColor={{false: '#767577', true: 'rgb(169,104,205)'}}
              thumbColor={'#f4f3f4'}
              onValueChange={toggleBonuses}
              value={useBonuses}
            />
          </View>
          <Text onPress={toggleBonuses}>Списать баллы</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: 10,
            marginVertical: 16,
            borderBottomColor: 'rgba(0, 0, 0, 0.12);',
            paddingBottom: 10,
            borderBottomWidth: 1,
          }}>
          <View style={styles.iconWrapper}>
            <Icon
              name="credit-card"
              type="FontAwesome5"
              style={{color: 'rgb(169,104,205)'}}
            />
          </View>
          <Text>Оплата картой **{appContext.cardTail}</Text>
        </View>
        <View style={{flexGrow: 1, flex: 0}}>
          <Button
            style={{margin: 8}}
            disabled={sum === 0}
            success={sum !== 0}
            block
            onPress={payOffline}>
            <Icon name="money-bill-wave" type="FontAwesome5" />
            <Text>Оплатить на кассе</Text>
          </Button>
          <Button
            disabled={needPassport || sum === 0}
            style={{margin: 8}}
            success={sum !== 0}
            block
            onPress={payOnline}>
            <Icon name="credit-card" type="FontAwesome5" />
            <Text>Оплатить онлайн</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

export default Checkout;

const styles = StyleSheet.create({
  iconWrapper: {width: 60, alignItems: 'center'},
  top: {flex: 0, flexDirection: 'row'},
  content: {flex: 1, flexDirection: 'column'},
  footer: {flex: 0},
});
