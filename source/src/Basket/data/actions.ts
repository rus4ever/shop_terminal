import {BasketData, BasketMethods} from './data';
import {netGET, stateToObject, netPOST} from 'utils';
import {ProductResponseProps} from './types';
import {Alert} from 'react-native';

export const fetchProductData = (stateObj: BasketData) => async (
  code: string,
) => {
  return netGET<ProductResponseProps>(
    `/api/barcode/${stateObj.clientId}/${code}`,
  );
};
export const loadQrCodeData = (stateObj: BasketData) => async (
  code: string,
) => {
  try {
    const res = await fetchProductData(stateObj)(code);
    addProductToBasket(stateObj)(res, code);
    return {error: undefined};
  } catch (e) {
    return {
      error: e,
    };
  }
};
const addProductToBasket = (stateObj: BasketData) => (
  item: ProductResponseProps,
  code: string,
) => {
  if (stateObj.products.find((obj) => obj.code === code)) {
    stateObj.products = stateObj.products.map((obj) =>
      obj.code === code ? {...obj, count: (obj.count || 0) + 1} : obj,
    );
  } else {
    stateObj.products = [...stateObj.products, {...item, code, count: 1}];
  }
};

const clearProductList = (stateObj: BasketData) => () => {
  stateObj.products = [];
};

const incrementProduct = (stateObj: BasketData) => (code: string) => {
  stateObj.products = stateObj.products.map((obj) => {
    if (obj.code === code && obj.countable) {
      return {...obj, count: (obj.count || 1) + 1};
    }
    return obj;
  });
};
const decrementProduct = (stateObj: BasketData) => (code: string) => {
  stateObj.products = stateObj.products
    .map((obj) => {
      if (obj.code === code && obj.countable && obj.count) {
        return {...obj, count: obj.count - 1};
      }
      return obj;
    })
    .filter((obj) => obj.count);
};

const checkout = (stateObj: BasketData) => async () => {
  const requestData = {
    payOffline: false,
    goods: stateObj.products.map(({code, count}) => ({barcode: code, count})),
  };

  const res = await netPOST<number>(
    `/api/checkout/${stateObj.clientId}`,
    requestData,
  );
  return res;
};

const enterShop = (stateObj: BasketData) => async (code: string) => {
  stateObj.shopAuthentificated = true;
};

export const actions = {
  enterShop,
  checkout,
  addProductToBasket,
  loadQrCodeData,
  clearProductList,
  incrementProduct,
  decrementProduct,
  fetchProductData,
};
