export interface ProductResponseProps {
  name: string;
  price: number;
  countable: boolean;
  passport: boolean;
}

export interface ProductProps extends ProductResponseProps {
  count?: number;
  code: string;
}
