import React from 'react';

export interface AppContextContent {
  userId: string;
  cardTail: string;
  bonusCount: number;
}
export const AppContext = React.createContext<null | AppContextContent>(null);
