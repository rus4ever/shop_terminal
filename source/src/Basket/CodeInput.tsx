import React, {useState, useEffect} from 'react';
import {StyleSheet, Image} from 'react-native';
import {BasketData, BasketMethods} from './data/data';
import {ProductResponseProps} from './data/types';
import {useHistory} from 'react-router-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Subtitle,
  Item,
  Input,
  CardItem,
} from 'native-base';

const CodeInput: React.FC<{
  data: BasketData & BasketMethods;
}> = (props) => {
  const [code, setCode] = useState('5060608742790');
  useEffect(() => {
    if (code.length > 12) {
      fetchItem(code);
    }
    setItem(null);
  }, [code]);

  const history = useHistory();
  const [item, setItem] = useState<ProductResponseProps | null>(null);
  const fetchItem = async (code: string) => {
    try {
      const res = await props.data.fetchProductData(code);
      setItem(res);
    } catch (e) {}
  };
  const goBack = () => history.goBack();
  const addItem = () => {
    if (item) {
      props.data.addProductToBasket(item, code);
      goBack();
    }
  };
  return (
    <Container>
      <Header>
        <Left>
          <Button transparent onPress={goBack}>
            <Icon name="arrow-back" />
            <Text>Назад</Text>
          </Button>
        </Left>
        <Body>
          <Title>Ввести </Title>
          <Subtitle>штрихкод</Subtitle>
        </Body>
        <Right />
      </Header>
      <Content>
        <Item rounded style={{marginTop: 24}}>
          <Icon active name="search" />
          <Input
            placeholder="Введите штрихкод"
            onChangeText={(text) => setCode(text)}
            value={code}
            keyboardType="number-pad"
          />
        </Item>
        {item && (
          <CardItem style={{flexDirection: 'column'}}>
            <Image source={require('../assets/images/burn.jpg')} />
            <Text>{item.name}</Text>
            <Text>{(item.price / 100).toFixed(2)} руб</Text>
          </CardItem>
        )}
        <Button style={{margin: 16}} block success onPress={addItem}>
          <Text>Добавить</Text>
        </Button>
      </Content>
    </Container>
  );
};

export default CodeInput;

const styles = StyleSheet.create({
  item: {
    alignItems: 'center',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    color: 'black',
    margin: 10,
  },
});
