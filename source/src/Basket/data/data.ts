import {ProductProps} from './types';
import {useState} from 'react';
import {stateToObject, bindMethods} from 'utils';
import {useBasketEffects} from './effects';
import {actions} from './actions';

export interface BasketData {
  products: ProductProps[];
  needPassport: boolean;
  clientId: string;
  legalAgePoped: boolean;
  shopAuthentificated: boolean;
}
export type BasketMethods = {
  [key in keyof typeof actions]: ReturnType<typeof actions[key]>;
};

export const usebasketData = (userId: string) => {
  const products = useState<BasketData['products']>([]);
  const needPassport = useState<BasketData['needPassport']>(false);
  const clientId = useState<BasketData['clientId']>(userId);
  const legalAgePoped = useState<BasketData['legalAgePoped']>(false);
  const shopAuthentificated = useState<BasketData['shopAuthentificated']>(
    false,
  );

  const stateObject = stateToObject<BasketData>({
    shopAuthentificated,
    products,
    needPassport,
    clientId,
    legalAgePoped,
  });
  const objWithMethods = bindMethods<BasketData, BasketMethods>(
    stateObject,
    actions,
  );
  useBasketEffects(objWithMethods);
  return objWithMethods;
};
