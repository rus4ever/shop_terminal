import React from 'react';
import {StyleSheet, Image, Alert} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Card,
  View,
  CardItem,
} from 'native-base';
import {BasketMethods, BasketData} from './data/data';
import ProductRow from './ProductRow';
import {useRouteMatch, useHistory} from 'react-router-native';

const BasketItems: React.FC<{data: BasketData & BasketMethods}> = (props) => {
  const {data} = props;
  const sum = (
    props.data.products.reduce(
      (acc, curr) => acc + (curr.count || 1) * curr.price,
      0,
    ) / 100
  ).toFixed(2);
  const count = props.data.products.reduce(
    (acc, curr) => (curr.count || 1) + acc,
    0,
  );
  const getEnding = () => {
    const remainder = count % 10;
    if (remainder === 1) {
      return '';
    }
    if (remainder > 1 && remainder <= 4) {
      return 'а';
    }
    return 'ов';
  };

  const {path} = useRouteMatch();
  const history = useHistory();
  const cancel = () => {
    Alert.alert(
      'Отменить покупку?',
      'Все неоплаченные продукты будут удалены из корзины',
      [
        {text: 'Отмена', style: 'cancel'},
        {
          text: 'Ок',
          style: 'destructive',
          onPress: () => {
            history.push('/');
          },
        },
      ],
    );
  };
  return (
    <Container>
      <Header>
        <Left>
          <Button transparent onPress={cancel}>
            <Icon name="arrow-back" />
            <Text>Отмена</Text>
          </Button>
        </Left>
        <Body>
          <Title>Корзина</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => history.push(`${path}/checkout`)}>
            <Text>Оплатить</Text>
          </Button>
        </Right>
      </Header>
      <Content>
        <Card style={{flexDirection: 'column', alignItems: 'center'}}>
          {data.products.length
            ? data.products.map((item) => (
                <ProductRow item={item} data={data} key={item.code} />
              ))
            : null}
          {!data.products.length && (
            <>
              <CardItem>
                <Text style={{fontSize: 24, marginBottom: 72, marginTop: 50}}>
                  Корзина пуста
                </Text>
              </CardItem>
              <CardItem cardBody>
                <Image
                  source={require('../assets/images/qr_empty_basket.png')}
                />
              </CardItem>
              <CardItem>
                <Text>Сканируйте товары на расстоянии ~15 см</Text>
              </CardItem>
            </>
          )}
        </Card>
      </Content>
      <View style={{flex: 0}}>
        {data.products.length > 0 ? (
          <>
            <Card
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 16,
              }}>
              <Text>
                {count} товар{getEnding()}
              </Text>

              <Text>ИТОГ {sum}p.</Text>
            </Card>
          </>
        ) : null}

        <Button
          style={{margin: 16}}
          block
          success
          onPress={() => {
            history.push(`${path}/scanner/add`);
          }}>
          <Text>Сканировать</Text>
        </Button>
      </View>
      <Footer>
        <FooterTab>
          <Button
            style={styles.footer}
            onPress={() => history.push(`${path}/input`)}>
            <Icon
              style={{color: '#333333'}}
              name="barcode"
              type="FontAwesome5"
            />
            <Text style={{color: '#888888'}}>Ввести код</Text>
          </Button>
          <Button
            style={styles.footer}
            onPress={() => history.push(`${path}/scanner/remove`)}>
            <Icon
              style={{color: '#333333'}}
              name="cart-remove"
              type="MaterialCommunityIcons"
            />
            <Text style={{color: '#888888'}}> Удалить</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
};

export default BasketItems;

const styles = StyleSheet.create({
  footer: {
    backgroundColor: '#ffffff',
  },
  sum: {
    height: 40,
    backgroundColor: 'green',
  },
  sumText: {
    color: 'white',
    fontSize: 32,
    alignSelf: 'center',
  },
});
