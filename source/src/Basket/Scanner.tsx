import React, {useRef, RefObject, useState, useEffect} from 'react';
import {StyleSheet, Alert, Dimensions, Vibration} from 'react-native';

import {RNCamera} from 'react-native-camera';
import {BasketData, BasketMethods} from './data/data';
import {useHistory, useParams} from 'react-router-native';
import {
  Container,
  Button,
  Icon,
  Text,
  Header,
  Left,
  Body,
  Title,
  Right,
  View,
  Card,
  Content,
} from 'native-base';

const {height, width} = Dimensions.get('window');

const BasketScanner: React.FC<{
  data: BasketData & BasketMethods;
  mode?: 'shopEnter';
}> = (props) => {
  const qrRef = useRef() as RefObject<RNCamera>;
  const [canAdd, setCanAdd] = useState(true);
  const history = useHistory();
  const mode = props.mode || (useParams() as {mode: 'add' | 'remove'}).mode;

  const add = async (code: string) => {
    const res = await props.data.loadQrCodeData(code);
    if (res.error === undefined) {
      history.goBack();
    } else {
      setTimeout(() => {
        Alert.alert(
          'Ошибка',
          'При распознавании штрихкода произошла ошибка, попробуйте еще раз',
          [
            {
              text: 'Ок',
              onPress: () => {
                setCanAdd(true);
              },
            },
          ],
        );
      }, 0);
    }
  };
  const shopEnter = async (code: string) => {
    await props.data.enterShop(code);
    history.goBack();
  };
  const remove = (code: string) => {
    props.data.products = props.data.products.filter((p) => p.code !== code);
    history.goBack();
  };
  const actions = {add, remove, shopEnter};
  const texts = {
    add: 'Отсканируйте штрих-код',
    remove: 'Отсканируйте штрих-код товара для удаления',
    shopEnter: 'Отсканируйте штрих-код на стойке у входа',
  };
  const onScan = (code: string) => {
    if (canAdd) {
      // RNBeep.beep();
      Vibration.vibrate(10);
      setCanAdd(false);
      actions[mode](code);
    }
  };
  return (
    <Container>
      <Header>
        <Left>
          <Button transparent onPress={() => history.goBack()}>
            <Icon name="arrow-back" />
            <Text>Назад</Text>
          </Button>
        </Left>
        <Body>
          <Title>Сканер</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        <View style={styles.hint}>
          <Text>{texts[mode]}</Text>
        </View>
        <RNCamera
          style={{width, height}}
          ref={qrRef}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          captureAudio={false}
          // permissionDialogTitle={'Permission to use camera'}
          // permissionDialogMessage={
          //   'We need your permission to use your camera phone'
          // }
          onBarCodeRead={(data) => {
            onScan(data.data);
          }}>
          <View style={styles.qrBorder}></View>
        </RNCamera>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  hint: {
    alignContent: 'center',
    backgroundColor: '#FFF1FF',
    fontSize: 24,
    paddingHorizontal: 35,
    paddingVertical: 16,
  },
  qrBorder: {
    flex: 0,
    flexDirection: 'column',
    justifyContent: 'space-around',
    width: width * 0.433333,
    height: height * 0.15,
    borderColor: 'green',
    borderWidth: 5,
    alignSelf: 'center',
    top: '40%',
  },
  topPanel: {
    flexDirection: 'row',
  },
});

export default BasketScanner;
