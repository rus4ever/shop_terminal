import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {
  Content,
  Header,
  Button,
  Text,
  Left,
  Icon,
  Container,
  Card,
  CardItem,
  Body,
  Title,
  Right,
} from 'native-base';
import {BasketData, BasketMethods} from './data/data';
import QRCode from 'react-native-qrcode-svg';
import {useHistory, useParams} from 'react-router-native';

const UserQr: React.FC<{data: BasketData & BasketMethods}> = (props) => {
  const {height, width} = Dimensions.get('window');
  const history = useHistory();
  const {mode} = useParams() as {mode: 'offline' | 'online'};
  console.log('mode', mode);
  const texts = {
    offline:
      'Поднесите QR код к сканеру кассы самообслуживания, чтобы оплатить товар',
    online:
      'Поднесите QR код к сканеру терминала онлайн оплаты, товар будет оплачен автоматически',
  };

  return (
    <Container>
      <Header>
        <Left>
          <Button transparent onPress={() => history.goBack()}>
            <Icon name="arrow-back" />
            <Text>Назад</Text>
          </Button>
        </Left>
        <Body>
          <Title>Оплата</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        <Card>
          <CardItem bordered>
            <QRCode
              size={Math.min(height, width) - 60}
              value={props.data.clientId}
            />
          </CardItem>
          <CardItem bordered>
            <Text>{texts[mode] || ''}</Text>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

export default UserQr;

const styles = StyleSheet.create({});
