import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';
import {
  NativeRouter,
  Route,
  Link,
  useHistory,
  Switch,
} from 'react-router-native';
import {StyleProvider} from 'native-base';
import Basket from './Basket/index';
import {MainScreen} from './MainScreen';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import {AppContext} from './Context/AppContext';

const MainApp = () => {
  const history = useHistory();
  const contextMockValue = {userId: '12345', cardTail: '4618', bonusCount: 50};
  return (
    <AppContext.Provider value={contextMockValue}>
      <StyleWrapper>
        <Switch>
          <Route path="/basket">
            <Basket />
          </Route>
          <Route path="/">
            <MainScreen />
          </Route>
        </Switch>
      </StyleWrapper>
    </AppContext.Provider>
  );
};

const StyleWrapper = (props) => {
  const theme = getTheme(material);
  if (Platform.OS === 'ios') {
    return <>{props.children}</>;
  } else {
    return <StyleProvider style={theme}>{props.children}</StyleProvider>;
  }
};

export default MainApp;
