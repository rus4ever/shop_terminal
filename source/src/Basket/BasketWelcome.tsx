import React, {useState} from 'react';
import {StyleSheet, Image} from 'react-native';
import {BasketData, BasketMethods} from './data/data';
import {
  Container,
  Text,
  Header,
  Left,
  Title,
  Right,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Body,
} from 'native-base';
import {
  Switch,
  Route,
  useParams,
  useHistory,
  useRouteMatch,
} from 'react-router-native';
import BasketScanner from './Scanner';

const BasketWelcome: React.FC<{data: BasketData & BasketMethods}> = (props) => {
  let {path} = useRouteMatch();
  const history = useHistory();
  const openScanner = () => {
    history.push(`${path}/scanner/shopEnter`);
  };
  return (
    <Switch>
      <Route path={`${path}/scanner/:mode`}>
        <BasketScanner data={props.data} />
      </Route>
      <Route path={path}>
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={() => history.push('/')}>
                <Icon name="arrow-back" />
                <Text>Отмена</Text>
              </Button>
            </Left>
            <Body>
              <Title>Корзина</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            <Card style={{alignItems: 'center'}}>
              <CardItem cardBody>
                <Image source={require('../assets/images/qr_welcome.png')} />
              </CardItem>
              <CardItem>
                <Text>
                  Отсканируйте QR-код у входа в магазин, чтобы начать покупки, и
                  QR-код на стойке у выхода, чтобы оплатить и завершить.
                </Text>
              </CardItem>
            </Card>
            <Button
              style={{margin: 8}}
              success
              block
              onPress={() => openScanner()}>
              <Text>Сканировать</Text>
            </Button>
          </Content>
        </Container>
      </Route>
    </Switch>
  );
};

export default BasketWelcome;

const styles = StyleSheet.create({});
