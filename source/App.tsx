/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';

import MainApp from './src/main';
import {NativeRouter} from 'react-router-native';

declare const global: {HermesInternal: null | {}};

const App = () => {
  return (
    <NativeRouter>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        <MainApp />
      </SafeAreaView>
    </NativeRouter>
  );
};

export default App;
