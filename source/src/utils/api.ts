import axios, {AxiosResponse} from 'axios';
export const BASE_URL = 'http://mandarincar.ru:8989';

let apiToken: string = '';

export const netGET = <T extends {}>(path: string) =>
  axios.get<T>(BASE_URL + path).then((res) => res.data);

export const netPOST = <T, D = {}>(path: string, data: D) => {
  return axios.post<T>(BASE_URL + path, data).then((res) => {
    return res.data;
  });
};
