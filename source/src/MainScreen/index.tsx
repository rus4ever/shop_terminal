import React, {useContext} from 'react';
import {Content, Card, Container, Text, Left, CardItem} from 'native-base';
import {useHistory} from 'react-router-native';
import {AppContext} from '../Context/AppContext';

export const MainScreen: React.FC<{userId: string}> = (props) => {
  const history = useHistory();
  const appContext = useContext(AppContext);
  return (
    <Container>
      <Content padder>
        <Card
          style={{
            marginHorizontal: 30,
            marginVertical: 20,
            padding: 16,
            backgroundColor: '#FFF1FF',
          }}>
          <CardItem
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: '#FFF1FF',
            }}>
            <Text>№ {appContext.userId}</Text>
            <Text>{appContext.bonusCount} баллов</Text>
          </CardItem>
        </Card>
        <Card
          onTouchEnd={() => history.push('/basket')}
          style={{
            borderColor: '#4CAF50',
            borderWidth: 2,
            marginHorizontal: 30,
            marginVertical: 20,
            padding: 16,
          }}>
          <CardItem style={{alignContent: 'center', flexDirection: 'column'}}>
            <Text>Новая закупка</Text>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};
