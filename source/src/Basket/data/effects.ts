import {BasketData, BasketMethods} from './data';
import {useEffect} from 'react';
import {Alert} from 'react-native';

export const useBasketEffects = (stateObj: BasketData & BasketMethods) => {
  useEffect(() => {
    if (
      !stateObj.legalAgePoped &&
      stateObj.products.some((product) => product.passport)
    ) {
      Alert.alert(
        'Внимание! 18+',
        'Продажа алкогольной продукции требует подтверждения возраста. При оплате обратитесь к консультанту у касс самообслуживания',
        [{text: 'ОК'}],
      );
      stateObj.legalAgePoped = true;
    }
  }, [stateObj.products]);
};
