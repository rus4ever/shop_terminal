import React from 'react';
import {StyleSheet, Text, View, Alert} from 'react-native';
import {ProductProps} from './data/types';
import {createConfigItem} from '@babel/core';
import {BasketData, BasketMethods} from './data/data';
import {CardItem, Button, Icon, Body} from 'native-base';

const ProductRow = (props: {
  item: ProductProps;
  data: BasketData & BasketMethods;
}) => {
  const decrementHandler = () => {
    if (!props.item.countable) {
      return;
    }
    if ((props.item.count || 1) > 1) {
      props.data.decrementProduct(props.item.code);
      return;
    }

    Alert.alert(
      'Подтвержение удаления товара',
      `Вы действительно хотите удалить ${props.item.name} из корзины`,
      [
        {text: 'Отмена', style: 'cancel'},
        {
          text: 'Ок',
          style: 'destructive',
          onPress: () => {
            props.data.decrementProduct(props.item.code);
          },
        },
      ],
    );
  };
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderBottomColor: 'rgba(120,120, 120, 1.0)',
        borderBottomWidth: 1,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Button
          small
          style={{margin: 0, padding: 0}}
          transparent
          onPress={decrementHandler}>
          <Icon name="minus" type="AntDesign" />
        </Button>
        <Text style={{margin: 0, padding: 0}}>{props.item.count || '1'}</Text>
        <Button
          small
          transparent
          onPress={() => props.data.incrementProduct(props.item.code)}>
          <Icon name="plus" type="AntDesign" />
        </Button>
      </View>
      <Body>
        <Text style={styles.name}> {props.item.name}</Text>
      </Body>
      <Text style={{marginLeft: 'auto', marginRight: 16}}>
        {(((props.item.count || 1) * props.item.price) / 100).toFixed(2)}р.
      </Text>
    </View>
  );
};

export default ProductRow;

const styles = StyleSheet.create({
  row: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    marginLeft: 0,
    marginRight: 16,
    marginTop: 12,
    marginBottom: 12,
  },
  counter: {
    alignItems: 'center',
    flex: 0,
    flexDirection: 'row',
  },
});
