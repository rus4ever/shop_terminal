import React, {useState} from 'react';
import {StyleSheet, FlatList, Alert} from 'react-native';

import {usebasketData} from './data/data';
import BasketScanner from './Scanner';
import ProductRow from './ProductRow';
import CodeInput from './CodeInput';
import Checkout from './Checkout';
import {Switch, Route, useRouteMatch, useHistory} from 'react-router-native';
import UserQr from './UserQr';
import BasketItems from './BasketItems';
import BasketWelcome from './BasketWelcome';

const Basket: React.FC<{userId: string}> = (props) => {
  const data = usebasketData(props.userId);
  const history = useHistory();
  let {path} = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/input`}>
        <CodeInput data={data} />
      </Route>
      <Route path={`${path}/scanner/:mode`}>
        <BasketScanner data={data} />
      </Route>
      <Route path={`${path}/checkout/pay/:mode`}>
        <UserQr data={data} />
      </Route>

      <Route path={`${path}/checkout`}>
        <Checkout data={data} />
      </Route>
      {!data.shopAuthentificated ? (
        <Route path={path}>
          <BasketWelcome data={data} />
        </Route>
      ) : (
        <Route path={path}>
          <BasketItems data={data} />
        </Route>
      )}
    </Switch>
  );
};

export default Basket;

const styles = StyleSheet.create({});
